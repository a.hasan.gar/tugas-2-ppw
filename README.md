# Tugas 2 PPW (Kelas E - Kelompok 4 - Paket B)

Pengembangan Aplikasi Web dengan TDD, OAuth, Webservice, Session & Cookies, dan Javascript

* * *

## Anggota Kelompok

- Agas Yanpratama (1606918396) - app 'comment' (NOT YET DONE, file on branch 'agas'. Tidak mau push ke master takut file dan belum ditest)
- Ahmad Hasan Siregar (1606892655) - app 'forum'
- Muhammad Fakhruddin Hafizh (1606875895) - app TO_BE_ADDED
- Zulia Putri Rahmadhani (1606918446) - app TO_BE_ADDED

## Status Pipelines & Code Coverage

[![pipeline status](https://gitlab.com/a.hasan.gar/tugas-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/a.hasan.gar/tugas-2-ppw/commits/master)
[![coverage report](https://gitlab.com/a.hasan.gar/tugas-2-ppw/badges/master/coverage.svg)](https://gitlab.com/a.hasan.gar/tugas-2-ppw/commits/master)

## Link Herokuapp

https://assignment-2-ppw.herokuapp.com/
